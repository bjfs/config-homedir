;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Uncomment and set this to your Emacs source dir if you can't describe
;; internal Emacs functions and variables in the code:
;; (setq find-function-C-source-directory "/Users/jdoe/build/emacs/src")

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Jane Doe"
      user-mail-address "jane@doe.local")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)


;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Set some shortcuts for text/enriched mode
(map! :leader
      (:prefix-map ("i" . "insert") :desc "Foreground color" "c" #'facemenu-set-foreground)
      (:prefix-map ("i" . "insert") :desc "Background color" "C" #'facemenu-set-background))

;; Elpher is a browser for the Gemini protocal, spiritual successor to Gopher (also suported)
(use-package elpher
  :config
  (setq gnutls-verify-error nil ;; not super secure, but should handle gemini better
        elpher-use-tls t))

;; Part of org-roam-ui contraption
(use-package! websocket
  :after org-roam)

;; org-mode roam stuff with web front-end (ui), very nice!
;; we add some handy shortcuts and set some sane defaults
(use-package org-roam-ui
  :after org-roam
  :config
  (map! :leader
        (:prefix-map ("n" . "notes")
         (:prefix ("r" . "roam")
          :desc "go back in roam ring" "o" #'org-mark-ring-goto
          :desc "launch org-roam-ui server" "s" #'org-roam-ui-open 
	 )))
  (setq al-mode 1
        org-roam-ui-sync-theme t
        org-roam-ui-follow t
        org-roam-ui-update-on-save t
        org-roam-ui-open-on-start t))

;; Give RCS some love, as it's historic thingy; which is disabled by default :/
(setq vc-handled-backends '(RCS SVN Git Hg)
      vc-rcs-checkin-switches "-l")
