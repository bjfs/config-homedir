import XMonad
import XMonad.Hooks.SetWMName
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.EZConfig
import XMonad.Util.Run

main = do
    xmproc <- spawnPipe "xmobar"
    xmonad . docks $ def
        -- Use "win" instead of "alt" for mod key
      { modMask            = mod4Mask
      , manageHook         = manageDocks <+> manageHook def
      , layoutHook         = avoidStruts  $ layoutHook def
      , logHook            = dynamicLogWithPP xmobarPP
          { ppOutput          = hPutStrLn xmproc
          , ppTitle           = xmobarColor "darkgreen"  "" . shorten 20
          , ppHiddenNoWindows = xmobarColor "grey" ""
          }
      , startupHook        = setWMName "BART"
      } `additionalKeys`
        -- Toggle xmobar via mod-b
      [ ((mod4Mask, xK_b), sendMessage ToggleStruts) ]
